import enum


class ButtonIndex(enum.Enum):
    LEFT_CLICK = 1
    MIDDLE_CLICK = 2
    RIGHT_CLICK = 3
    SCROLL_UP = 4
    SCROLL_DOWN = 5
    DOUBLE_LEFT_CLICK = 6
    DOUBLE_MIDDLE_CLICK = 7
    DOUBLE_RIGHT_CLICK = 8


def _format_tags(tag, param, string):
    return '%{{{0}{1}}}{2}%{{{0}-}}'.format(tag, param, string)


def _activation_tags(tag, string):
    return '%{{+{0}}}{1}%{{-{0}}}'.format(tag, string)


def fg(color, string):
    return _format_tags('F', color, string)


def bg(color, string):
    return _format_tags('B', color, string)


def r(string):
    return '%{{R}}{}%{{R}}'.format(string)


def u(string):
    return _activation_tags('u', string)


def ucolor(color, string):
    return _format_tags('u', color, string)


def o(string):
    return _activation_tags('o', string)


def ocolor(color, string):
    return _format_tags('o', color, string)


def font(font_index, string):
    return _format_tags('F', font_index, string)


def offset(off):
    return '%{{O{}}}'.format(off)


def action(button_index, command, string):
    return '%{{A{}:{}:}}{}%{{A}}'.format(
        button_index.value if isinstance(button_index, ButtonIndex) else
        button_index, command.replace(':', '\\:'), string)
